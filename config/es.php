<?php
/**
 * Author: XiaoFei Zhai
 * Date: 2017/11/15
 * Time: 14:59
 */
return [
    'hosts' => [
        '127.0.0.1:9200',         // IP + Port
    ],
    'index' =>'goods'
];